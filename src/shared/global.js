import { createGlobalStyle, css } from 'styled-components';

export const bodyStyles = css`
  font-size: 16px;
  color: red;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  -webkit-tap-highlight-color: transparent;
  -webkit-overflow-scrolling: touch;
  * {
    box-sizing: border-box;
  }
`;

export const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap');

 body {
   ${bodyStyles}
   margin: 0;
   font-family: 'Roboto', sans-serif;
   overflow-y: auto;
   overflow-x: hidden;
 }
`;