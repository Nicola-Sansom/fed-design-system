const Colors = {
  BRAND: '#d3072a',
  PRIMARY_DARK: '#333',
  PRIMARY_LIGHT: '#fff'
}

export default Colors;